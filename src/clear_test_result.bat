pushd %~dp0
del "*.diff" /S /Q 
del "*.result.*" /S /Q 
del "*.zdiff.*" /S /Q 
del "*.zorig.*" /S /Q 
del "*.tmp" /S /Q 
popd
