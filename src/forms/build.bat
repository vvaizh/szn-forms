del built\szn.js
node optimizers\r.js -o conf\build-szn.js

call :szn_lib_in_txt
exit

rem -----------------------------------------------------------
:szn_lib_in_txt
del built\szn.lib.in.txt
copy forms\wbt.lib.txt built\szn.lib.in.txt
copy /B built\szn.lib.in.txt + forms\szn\in.lib.txt built\szn.lib.in.txt
copy /B built\szn.lib.in.txt + forms\szn\quota\tests\cases\in.lib.txt built\szn.lib.in.txt
copy /B built\szn.lib.in.txt + forms\szn\need\vacancy\tests\cases\in.lib.txt built\szn.lib.in.txt
copy /B built\szn.lib.in.txt + forms\szn\need\garanties\tests\cases\in.lib.txt built\szn.lib.in.txt
copy /B built\szn.lib.in.txt + forms\szn\need\main\tests\cases\in.lib.txt built\szn.lib.in.txt
copy /B built\szn.lib.in.txt + forms\szn\reserve\tests\cases\in.lib.txt built\szn.lib.in.txt
exit /B
