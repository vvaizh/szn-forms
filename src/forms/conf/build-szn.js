// node optimizers\r.js -o baseUrl=. name=optimizers/almond include=forms/fms/extension out=..\built\fms.js wrap=true
({
    baseUrl: "..",
    include: ['forms/szn/e_szn'],
    name: "optimizers/almond",
    out: "..\\built\\szn.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/text'
      }
    }
})