﻿define(function ()
{
	var helper =
	{
		GetNumbering: function (count, n1, n2, n10)
		{
			return ((count % 10 == 1 && count % 100 != 11) ? n1 :
					((count % 10 >= 2 && count % 10 <= 4 && (count % 100 < 10 || count % 100 >= 20)) ? n2 : n10));
		}

		, GetNumberingBytes: function (count)
		{
			return helper.GetNumbering(count, 'байт', 'байта', 'байт');
		}

		, FMSPrepareFileName: function (prefix, date_of_creation)
		{
			if (null == prefix)
			{
				return '';
			}
			else
			{
				var inn = '';

				try { inn = '-' + app.user.inn; } catch (ex) { }

				function pad2(n) { return (n < 10 ? '0' : '') + n; }

				var txt_creation =
							date_of_creation.getFullYear() +
					pad2(date_of_creation.getMonth() + 1) +
					pad2(date_of_creation.getDate()) +
					pad2(date_of_creation.getHours()) +
					pad2(date_of_creation.getMinutes()) +
					pad2(date_of_creation.getSeconds());

				return prefix + '-' + txt_creation + inn + '-0000';
			}
		}

		, GetSizeOfUtf8: function(str)
		{
			var m;
			m = encodeURIComponent(str).match(/%[89ABab]/g);
			return str.length + (m != null ? m.length : 0);
		}
	};
	return helper;
});