define([
	  'forms/test/tpl/controller'
	, 'forms/test/xml/codec.xml'
],
function (BaseTplController, codec_xml)
{
	return function ()
	{
		var res = BaseTplController();
		res.UseCodec(codec_xml);
		return res;
	}
});
