﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/szn/need/vacancy/e_vacancy.html'
],
function (c_binded, tpl)
{
	return function ()
	{
		var controller = c_binded(tpl);

		return controller;
	}
});
