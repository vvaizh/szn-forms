include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\vacancy\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

shot_check_png ..\..\shots\00new.png

wait_click_text "добавить вакансию"
play_stored_lines vacancy_fields_1
wait_click_full_text "Сохранить"

shot_check_png ..\..\shots\02sav_1.png

wait_click_text "добавить вакансию"
play_stored_lines vacancy_fields_2
shot_check_png ..\..\shots\02sav_0.png
wait_click_full_text "Сохранить"
wait_text "9:00"

shot_check_png ..\..\shots\02sav.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02sav.json.result.txt

wait_click_full_text "Редактировать отчёт"
shot_check_png ..\..\shots\02sav.png

wait_click_text "столяр"
js wbt_SetModelFieldValue("Профессия", "маляр");
wait_click_full_text "Сохранить"

shot_check_png ..\..\shots\02sav_2.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02sav_2.json.result.txt

exit