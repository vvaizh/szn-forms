﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/szn/need/vacancies/e_vacancies.html'
	, 'forms/base/b_collection'
	, 'forms/base/h_msgbox'
	, 'forms/szn/need/vacancy/c_vacancy'
],
function (c_binded, tpl, b_collection, h_msgbox, c_vacancy)
{
	return function ()
	{
		var is_empty= function(v)
		{
			var res = (!v || '' == v);
			return res;
		}

		var BuildClassesForItem= function(model,i_item)
		{
			var res = '';
			if (model)
			{
				var item = model[i_item]
				if (is_empty(item.По_результатам_конкурса))
					res += ' no-competition';
				if (is_empty(item.Социальные_гарантии))
					res += ' no-garanties';
				if (is_empty(item.Дополнительные_пожелания))
					res += ' no-wishes';
				if (is_empty(item.Квалификационные_требования))
					res += ' no-requirements';
				if (is_empty(item.Характер_работы))
					res += ' no-workkind';
				if (is_empty(item.Режим_работы))
					res += ' no-workmode';
				if (is_empty(item.Квалификация))
					res += ' no-qualification';
			}
			return res;
		}

		var item_modal_controller= function(model_item, title, on_after_save)
		{
			var controller =
				{
					ShowModal: function(e)
					{
						var controller = c_vacancy();
						controller.SetFormContent(model_item);
						var bname_Ok = 'Сохранить';
						h_msgbox.ShowModal({
							controller: controller
							, width: 880
							, id_div: 'cpw-forms-szn-need-vacancy'
							, height: 610
							, title: title
							, buttons: [bname_Ok, 'Отменить']
							, onclose: function (bname)
							{
								if (bname_Ok == bname)
									on_after_save(controller.GetFormContent());
							}
						});
					}
				};
			return controller;
		}

		var controller = c_binded
		(
			tpl
			,{
				CreateBinding: b_collection
				, BuildClassesForItem: BuildClassesForItem
				, item_modal_controller: item_modal_controller
				, h_msgbox: h_msgbox
			}
		);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			$(sel + ' button.delete').button({ icon: "ui-icon-circle-minus", iconPosition: { iconPositon: "end" } });
			$(sel + ' button.add').button({ icon: "ui-icon-circle-plus", iconPosition: { iconPositon: "end" } });
		}

		return controller;
	}
});
