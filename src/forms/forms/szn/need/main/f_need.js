define([
	  'forms/szn/need/main/c_need_ce'
	, 'forms/szn/need/main/ff_need'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'need'
		, Title: 'Сведения о потребности в работниках, наличии свободных рабочих мест (вакантных должностей)'
		, FileFormat: FileFormat
	};
	return form_spec;
});
