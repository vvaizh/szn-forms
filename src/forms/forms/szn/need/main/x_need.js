﻿define([
	  'forms/base/codec/codec.xml'
	, 'forms/base/codec/codec.json.base'
],
function (codec_xml, codec_json_base)
{
	return function ()
	{
		var xml_codec = codec_json_base(codec_xml());
		xml_codec.schema =
			{
				tagName: 'Сведения_о_потребности_в_работниках'
				,fields:
					{
						'Социальные_гарантии':
							{
								type: 'object'
							}
						,'Вакансии':
							{
								type: 'array'
								, item: { tagName: 'Вакансия' }
							}
					}
			};
		return xml_codec;
	}
});
