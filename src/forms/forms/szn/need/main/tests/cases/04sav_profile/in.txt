include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\vacancy\tests\cases\in.lib.txt quiet
include ..\..\..\..\garanties\tests\cases\in.lib.txt quiet
include ..\in.lib.txt quiet
include ..\..\..\..\..\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
shot_check_png ..\..\shots\00new.png

execute_javascript_stored_lines fill_szn_profile_1

wait_click_text "Обновить данными из профиля"

play_stored_lines need_header_fields_1

click_text "Не указаны"
play_stored_lines garanties_fields_1
wait_click_full_text "Сохранить"

wait_click_text "добавить вакансию"
play_stored_lines vacancy_fields_1
wait_click_full_text "Сохранить"

wait_click_text "добавить вакансию"
play_stored_lines vacancy_fields_2
wait_click_full_text "Сохранить"

shot_check_png ..\..\shots\02sav_profile.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\04sav_profile.result.xml
exit