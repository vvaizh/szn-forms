﻿define([
	  'forms/szn/econtent/c_econtent'
	, 'forms/szn/need/main/c_need'
	, 'forms/szn/need/main/ff_need'
],
function (BaseController, BaseFormController, formSpec)
{
	return function ()
	{
		var controller = BaseController(BaseFormController, formSpec);

		controller.GetCoveringLetterText = function ()
		{
			return 'Направляем документ\r\n' +
				'   "Сведения о потребности в работниках, наличии свободных рабочих мест (вакантных должностей)"';
		}

		controller.GetCoveringLetterAttachmentName = function ()
		{
			return 'Отчёт о потребности в работниках';
		}

		controller.GetCoveringLetterAttachmentNote = function ()
		{
			return 'Сведения о потребности в работниках, наличии свободных рабочих мест (вакантных должностей)';
		}

		return controller;
	}
});