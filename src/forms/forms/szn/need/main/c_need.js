﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/szn/need/main/e_need.html'
	, 'forms/szn/need/vacancies/c_vacancies'
	, 'forms/base/codec/codec.xml'
	, 'forms/base/codec/codec.json.base'
	, 'forms/szn/need/garanties/c_garanties_modal'
	, 'tpl!forms/szn/need/main/v_need.xaml'
	, 'forms/base/codec/codec.tpl.xaml'
	, 'forms/szn/need/main/x_need'
	, 'forms/szn/need/garanties/h_garanties'
	, 'tpl!forms/szn/need/main/p_need.html'
	, 'forms/szn/base/h_profile'
],
function (c_binded, tpl, c_vacancies, codec_xml, codec_json_base, c_garanties_modal, print_tpl, codec_tpl_xaml, x_need, h_garanties, profiled_tpl, h_profile)
{
	return function ()
	{
		var controller = c_binded(tpl,
			{
				field_spec:
					{
						Вакансии: { controller: c_vacancies }
						, Социальные_гарантии: c_garanties_modal
					}
			}
		);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			if (!this.model)
				this.model = {};
			base_Render.call(this, sel);
		}

		var base_GetFormContent = controller.base_GetFormContent;
		controller.BuildXamlView = function ()
		{
			var model = !this.binding ? this.model : base_GetFormContent.call(this);
			var content = print_tpl({
				  form: model
				, garanties_to_text: h_garanties.garanties_to_text
				, text_report_period: h_profile.text_report_period
			});
			var codec = codec_tpl_xaml();
			return codec.Encode(content);
		}

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return profiled_tpl({ form: this.model, text_report_period: h_profile.text_report_period });
		}

		controller.UseProfile = function (profile)
		{
			if (profile)
			{
				if (!this.model)
					this.model = {};
				if (!this.model.Абонент)
					this.model.Абонент = {};
				if (profile.Abonent)
				{
					var abonent = profile.Abonent;
					if (abonent.Name && !this.model.Абонент.Наименование)
						this.model.Абонент.Наименование = abonent.Name;
					if (abonent.OKVED)
					{
						if (!this.model.Абонент.ОКВЭД)
							this.model.Абонент.ОКВЭД = abonent.OKVED;
					}
					if (abonent.OKOPF)
					{
						if (!this.model.Абонент.ОКОПФ)
							this.model.Абонент.ОКОПФ = abonent.OKOPF;
					}
				}
				if (profile.User)
				{
					if (!this.model.Абонент.Представитель_работодателя)
						this.model.Абонент.Представитель_работодателя = h_profile.text_report_fio(profile.User);
				}
				if (profile.Report)
				{
					if (!this.model.Отчёт)
						this.model.Отчёт = {};
					this.model.Отчёт.За = h_profile.report_period(profile.Report);
				}
				if (this.binding)
					this.binding.model = this.model;
			}
		}

		controller.UseCodec(x_need());

		return controller;
	}
});
