﻿define([
	  'forms/szn/need/garanties/c_garanties'
	, 'forms/base/h_msgbox'
	, 'forms/szn/need/garanties/h_garanties'
],
function (c_garanties, h_msgbox, h_garanties)
{
	return function(model)
	{
		var res =
			{
				text: function (item) { return h_garanties.garanties_to_text(item, 'Не указаны'); }
				,controller: function (item, onAfterSave)
				{
					var controller =
					{
						ShowModal: function (e)
						{
							e.preventDefault();
							var controller = c_garanties();
							if (model && model.Социальные_гарантии)
								controller.SetFormContent(model.Социальные_гарантии);
							var bname_Ok = 'Сохранить';
							h_msgbox.ShowModal({
								controller: controller
								, width: 540
								, height: 250
								, id_div: 'cpw-szn-need-garanties'
								, title: 'Социальные гарантии работникам'
								, buttons: [bname_Ok, 'Отменить']
								, onclose: function (bname)
								{
									if (bname_Ok == bname)
									{
										model.Социальные_гарантии = controller.GetFormContent();
										onAfterSave();
									}
								}
							});
						}
					};
					return controller;
				}
			}
		return res;
	}
});
