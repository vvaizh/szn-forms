﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/szn/need/garanties/e_garanties.html'
],
function (c_binded, tpl)
{
	return function ()
	{
		var controller = c_binded(tpl);

		return controller;
	}
});
