﻿define(
function ()
{
	var garanties_to_text = function (item, text_for_empty)
	{
		var garanties = [];
		if (item)
		{
			if (item.Медицинские && 'true' == item.Медицинские.toString())
				garanties.push('медицинское обслуживание');
			if (item.Cанаторные && 'true' == item.Cанаторные.toString())
				garanties.push('санаторно-курортное обеспечение');
			if (item.Детские && 'true' == item.Детские.toString())
				garanties.push('обеспечение детскими дошкольными учреждениями');
			if (item.Обеденные && 'true' == item.Обеденные.toString())
				garanties.push('условия приема пищи во время перерыва');
		}
		return 0 == garanties.length ? text_for_empty : garanties.join(', ');
	}
	return { garanties_to_text: garanties_to_text };
});
