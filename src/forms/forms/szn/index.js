define
(
	[
		  'forms/collector'
		, 'forms/szn/quota/c_quota'
		, 'forms/szn/quota/c_quota_ce'
		, 'forms/szn/reserve/c_reserve'
		, 'forms/szn/need/vacancy/c_vacancy'
		, 'forms/szn/need/vacancies/c_vacancies'
		, 'forms/szn/need/main/c_need'
		, 'forms/szn/need/garanties/c_garanties'
	],
	function (collect)
	{
		return collect([
		  'quota'
		, 'quota_ce'
		, 'reserve'
		, 'vacancy'
		, 'vacancies'
		, 'need'
		, 'garanties'
		], Array.prototype.slice.call(arguments,1));
	}
);