﻿define(
function ()
{
	var text_report_period= function (За)
	{
		if (За)
		{
			switch (За.Период)
			{
				case 'Год': return За.Год;
				case '9 месяцев': return За.Девять_месяцев + ' 9 месяцев ' + За.Год + ' года';
				case 'Полугодие': return За.Полугодие + ' Полугодие ' + За.Год + ' года';
				case 'Квартал': return За.Квартал + ' Квартал ' + За.Год + ' года';
				case 'Месяц': return За.Месяц + ' ' + За.Год + ' года';
				case 'Декада': return За.Декада + ' декада ' + За.Месяц + ' ' + За.Год + ' года';
				case 'Сутки': return За.День;
				default: return '';
			}
		}
	}
	var text_report_fio= function(user)
	{
		var name = '';
		if (user.LastName)
			name += user.LastName;
		if (user.FirstName)
			name += ' ' + user.FirstName[0] + '.'
		if (user.MiddleName)
			name += user.MiddleName[0] + '.'
		return name;
	}
	var report_period= function(report)
	{
		var period = { Период: report.Период };
		switch (report.Период)
		{
			case 'Год':
				period.Год = report.Год;
				break;
			case '9 месяцев':
				period.Год = report.Год;
				period.Девять_месяцев = report.Девять_месяцев;
				break;
			case 'Полугодие':
				period.Год = report.Год;
				period.Полугодие = report.Полугодие;
				break;
			case 'Квартал':
				period.Год = report.Год;
				period.Квартал = report.Квартал;
				break;
			case 'Месяц':
				period.Год = report.Год;
				period.Месяц = report.Месяц;
				break;
			case 'Декада':
				period.Год = report.Год;
				period.Месяц = report.Месяц;
				period.Декада = report.Декада;
				break;
			case 'Сутки':
				period.День = report.День;
				break;
		}
		return period;
	}
	return { text_report_period: text_report_period, text_report_fio: text_report_fio, report_period: report_period };
});
