define
(
	[
		  'forms/collector'
		, 'txt!forms/szn/quota/tests/contents/01sav.etalon.xml'
		, 'txt!forms/szn/reserve/tests/contents/01sav.etalon.xml'
		, 'txt!forms/szn/need/vacancy/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/szn/need/vacancies/tests/contents/test1.json.txt'
		, 'txt!forms/szn/need/main/tests/contents/test1.json.txt'
		, 'txt!forms/szn/need/garanties/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/szn/need/vacancies/tests/contents/02sav_2.json.etalon.txt'
		, 'txt!forms/szn/need/main/tests/contents/02sav.etalon.xml'
	],
	function (collect)
	{
		return collect([
		  'szn_quota_01sav'
		, 'szn_reserve_01sav'
		, 'szn_vacancy_01sav'
		, 'szn_vacancies_test1'
		, 'szn_need_test1'
		, 'szn_garanties_01sav'
		, 'szn_vacancies_02sav_2'
		, 'szn_need_02sav'
		], Array.prototype.slice.call(arguments,1));
	}
);