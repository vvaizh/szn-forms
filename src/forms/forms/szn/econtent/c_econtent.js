﻿define([
	  'forms/base/log'
	, 'forms/base/h_file'
	, 'forms/base/h_times'
],
function (
	  GetLogger
	, h_file
	, h_times
	)
{
	return function (BaseFormController, formSpec)
	{
		var log = GetLogger('c_econtent');
		var controller = BaseFormController();

		var form = formSpec;

		controller.ShowAttachmentsPanel = false;

		controller.result_Data =
		{
			subject: '',
			body: '',
			attachments: []
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var content = base_GetFormContent();
			var filename = h_file.FMSPrepareFileName(form.FilePrefix, h_times.safeDateTime()) + '.' + form.FileExtension;
			var attachments =
				[{
					Name: this.GetCoveringLetterAttachmentName(),
					FileName: filename,
					FilePath: filename,
					FileSize: h_file.GetSizeOfUtf8(content),
					Note: this.GetCoveringLetterAttachmentNote(),
					Content: content,
					AttachmentType: 'f'
				}];
			this.result_Data.attachments = attachments;
			this.result_Data.body = this.GetCoveringLetterText();
			return this.result_Data;
		};

		var base_SetFormContent = controller.SetFormContent;
		controller.SetFormContent = function (form_content)
		{
			var content = 'object' == (typeof form_content) ? form_content : JSON.parse(form_content);
			var res = base_SetFormContent.call(controller, !content.attachments || content.attachments.length < 1 ? '' : content.attachments[0].Content);
			return res;
		};

		var base_CopyFormContent = controller.CopyFormContent;
		controller.CopyFormContent = function (form_content)
		{
			var content = 'object' == (typeof form_content) ? form_content : JSON.parse(form_content);
			base_CopyFormContent.call(controller, !content.attachments || content.attachments.length < 1 ? '' : content.attachments[0].Content);
		}

		return controller;
	}
});