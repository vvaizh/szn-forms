﻿define([
	  'forms/szn/econtent/c_econtent'
	, 'forms/szn/reserve/c_reserve'
	, 'forms/szn/reserve/ff_reserve'
],
function (BaseController, BaseFormController, formSpec)
{
	return function ()
	{
		var controller = BaseController(BaseFormController, formSpec);

		controller.GetCoveringLetterText = function ()
		{
			return 'Направляем документ\r\n' +
				'"Информация о резервировании рабочих мест для трудоустройства граждан, испытывающих трудности в поиске работы"'
		}

		controller.GetCoveringLetterAttachmentName = function ()
		{
			return 'Информация о резервировании';
		}

		controller.GetCoveringLetterAttachmentNote = function ()
		{
			return 'Информация о резервировании рабочих мест для трудоустройства граждан, испытывающих трудности в поиске работы';
		}

		return controller;
	}
});