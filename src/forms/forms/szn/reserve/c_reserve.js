﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/szn/reserve/e_reserve.html'
	, 'forms/base/codec/codec.xml'
	, 'tpl!forms/szn/reserve/v_reserve.xaml'
	, 'forms/base/codec/codec.tpl.xaml'
	, 'forms/szn/base/h_profile'
	, 'tpl!forms/szn/reserve/p_reserve.html'
],
function (c_binded, tpl, codec_xml, print_tpl, codec_tpl_xaml, h_profile, profiled_tpl)
{
	return function ()
	{
		var controller = c_binded(tpl);

		var tooltip_content= function ()
		{
			var element = $(this);
			var title = element.attr("title");
			if (!title || '' == title)
			{
				var tr = element.parents('tr');
				title = tr.attr("row-title");
				var icol = element.index();
				if (0 != icol)
				{
					var table = tr.parents('table');
					var head_row = $(table.find('tr')[0]);
					var head_td = $(head_row.find('td')[icol]);
					var col_title = head_td.attr('title');
					title = col_title + '<hr/>' + title;
				}
			}
			return title;
		}

		var base_Render = controller.Render;
		controller.Render = function(sel)
		{
			base_Render.call(this, sel);
			$(sel).tooltip({items: "td, tr", content: tooltip_content });
			var self = this;

			var inputs = $(sel + ' input');
			for (var i= 0; i<inputs.length; i++)
			{
				var input = $(inputs[i]);
				var model_field_name = input.attr('model_field_name');
				if (0!=model_field_name.indexOf('Всего'))
				{
					input.on('input', function () { self.CalcTotal(); });
					input.change(function () { self.CalcTotal(); });
				}
			}
		}

		var base_GetFormContent = controller.GetFormContent;

		controller.BuildXamlView = function ()
		{
			var model = !this.binding ? this.model : base_GetFormContent.call(this);
			var content = print_tpl({
				form: model, text_report_period: h_profile.text_report_period
			});
			var codec = codec_tpl_xaml();
			return codec.Encode(content);
		}

		controller.CalcTotal= function()
		{
			var sel = this.binding.form_div_selector;
			var Мест = 0;
			var Трудоустроено = 0;
			var inputs = $(sel + ' input');
			for (var i = 0; i < inputs.length; i++)
			{
				var input = $(inputs[i]);
				var model_field_name = input.attr('model_field_name');
				if (0 != model_field_name.indexOf('Всего'))
				{
					try
					{
						var num = parseInt(input.val());
						if ('NaN' != num.toString())
						{
							if (-1 != model_field_name.indexOf('Мест'))
								Мест += num;
							if (-1 != model_field_name.indexOf('Трудоустроено'))
								Трудоустроено += num;
						}
					}
					catch (ex){}
				}
			}
			if ('NaN' != Мест.toString())
				$(sel + ' input[model_field_name="Всего.Мест"]').val(Мест);
			if ('NaN' != Трудоустроено.toString())
				$(sel + ' input[model_field_name="Всего.Трудоустроено"]').val(Трудоустроено);
		}

		controller.UseProfile = function (profile)
		{
			if (profile)
			{
				if (!this.model)
					this.model = {};
				if (!this.model.Абонент)
					this.model.Абонент = {};
				if (profile.Abonent)
				{
					var abonent = profile.Abonent;
					if (abonent.Name && !this.model.Абонент.Наименование)
						this.model.Абонент.Наименование = abonent.Name;
				}
				if (profile.User)
				{
					if (!this.model.Абонент.Руководитель)
						this.model.Абонент.Руководитель = h_profile.text_report_fio(profile.User);
				}
				if (profile.Recipients)
					this.model.Контролирующий_орган = profile.Recipients[0].Name;
				if (profile.Report)
				{
					if (!this.model.Отчёт)
						this.model.Отчёт = {};
					this.model.Отчёт.За = h_profile.report_period(profile.Report);
				}
				if (this.binding)
					this.binding.model = this.model;
			}
		}

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return profiled_tpl({ form: this.model, text_report_period: h_profile.text_report_period });
		}

		var xml_codec = codec_xml();
		xml_codec.schema = { tagName: 'Резервирование_рабочих_мест_для_испытывающих_трудности' };
		controller.UseCodec(xml_codec);

		return controller;
	}
});
