define([
	  'forms/szn/reserve/c_reserve_ce'
	  ,'forms/szn/reserve/ff_reserve'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'reserve'
		, Title: 'Информация о резервировании рабочих мест для трудоустройства граждан, испытывающих трудности в поиске работы'
		, FileFormat: FileFormat
	};
	return form_spec;
});
