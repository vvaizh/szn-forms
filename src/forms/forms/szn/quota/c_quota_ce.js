﻿define([
	  'forms/szn/econtent/c_econtent'
	, 'forms/szn/quota/c_quota'
	, 'forms/szn/quota/ff_quota'
],
function (BaseController, BaseFormController, formSpec)
{
	return function ()
	{
		var controller = BaseController(BaseFormController, formSpec);

		controller.GetCoveringLetterText = function ()
		{
			return 'Направляем документ\r\n' +
				'   "Информация о выполнении квоты для приема на работу инвалидов"\r\n' +
				' по форме, утверждённой приказом\r\n' +
				' Министерства труда и миграционной  политики Удмуртской республики\r\n' + 
				' от «14» 04 2017 года № 01-05/025';
		}

		controller.GetCoveringLetterAttachmentName = function ()
		{
			return 'Информация о выполнении квоты';
		}

		controller.GetCoveringLetterAttachmentNote = function ()
		{
			return 'Информация о выполнении квоты для приема на работу инвалидов';
		}

		return controller;
	}
});