define([
	  'forms/szn/quota/c_quota_ce'
	  ,'forms/szn/quota/ff_quota'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'quota'
		, Title: 'Информация о выполнении квоты для приёма на работу инвалидов'
		, FileFormat: FileFormat
	};
	return form_spec;
});
