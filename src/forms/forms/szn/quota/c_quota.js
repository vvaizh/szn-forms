﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/szn/quota/e_quota.html'
	, 'tpl!forms/szn/quota/v_quota.xaml'
	, 'forms/base/codec/codec.tpl.xaml'
	, 'forms/base/codec/codec.xml'
	, 'tpl!forms/szn/quota/p_quota.html'
	, 'forms/szn/base/h_profile'
],
function (c_binded, tpl, print_tpl, codec_tpl_xaml, codec_xml, profiled_tpl, h_profile)
{
	return function ()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render = function(sel)
		{
			base_Render.call(this, sel);
			var self = this;

			var iарендованных = $(sel + ' input[model_field_name="Количество_квотируемых.арендованных"]');
			iарендованных.on('input', function () { self.UpdateVisibility(); });
			iарендованных.change(function () { self.UpdateVisibility(); });

			var iквотируемых= $(sel + ' input[model_field_name="Количество_квотируемых.всего"]');
			iквотируемых.on('input', function () { self.UpdateDiff(); });
			iквотируемых.change(function () { self.UpdateDiff(); });

			var iработающих= $(sel + ' input[model_field_name="Количество_работающих_инвалидов.всего"]');
			iработающих.on('input', function () { self.UpdateDiff(); });
			iработающих.change(function () { self.UpdateDiff(); });
		}

		var base_GetFormContent = controller.GetFormContent;

		controller.BuildXamlView= function()
		{
			var model = !this.binding ? this.model : base_GetFormContent.call(this);
			var content = print_tpl({
				form: model, text_report_period: h_profile.text_report_period
			});
			var codec = codec_tpl_xaml();
			return codec.Encode(content);
		}

		controller.SafeCopyFieldFromProfile= function(model, profile, profile_field_name, model_field_name)
		{
			if (!model_field_name)
				model_field_name = profile_field_name;
			if (profile[profile_field_name] && !model[model_field_name])
				model[model_field_name] = profile[profile_field_name];
		}

		controller.UseProfile = function (profile)
		{
			if (profile)
			{
				if (!this.model)
					this.model = {};
				if (!this.model.Абонент)
					this.model.Абонент = {};
				if (profile.Abonent)
				{
					var abonent = profile.Abonent;
					this.SafeCopyFieldFromProfile(this.model.Абонент, abonent, 'Name', 'Наименование');
					this.SafeCopyFieldFromProfile(this.model.Абонент, abonent, 'Inn', 'ИНН');
					this.SafeCopyFieldFromProfile(this.model.Абонент, abonent, 'Kpp', 'КПП');
					if (abonent)
						this.SafeCopyFieldFromProfile(this.model.Абонент, abonent.OKOPF, 'code', 'ОКОПФ');
					if (abonent.OKVED)
					{
						var okved = abonent.OKVED.replace(",", ";");
						var parts = okved.split(";");
						if (!this.model.Абонент.ОКВЭД && parts && 0 < parts.length)
							this.model.Абонент.ОКВЭД = parts[0];
					}
				}
				if (profile.User)
				{
					if (!this.model.Абонент.Руководитель)
						this.model.Абонент.Руководитель = h_profile.text_report_fio(profile.User);
				}
				if (profile.Recipients)
					this.model.Контролирующий_орган = profile.Recipients[0].Name;
				if (profile.Report)
				{
					if (!this.model.Отчёт)
						this.model.Отчёт = { };
					this.model.Отчёт.За = h_profile.report_period(profile.Report);
				}
				if (this.binding)
					this.binding.model = this.model;
			}
		}

		controller.UpdateDiff= function()
		{
			var sel = this.binding.form_div_selector;

			try
			{
				var iквотируемых = $(sel + ' input[model_field_name="Количество_квотируемых.всего"]');
				var iработающих = $(sel + ' input[model_field_name="Количество_работающих_инвалидов.всего"]');
				var iневыполнено = $(sel + ' input[model_field_name="Невыполненная_квота.всего"]');

				var квотируемых = parseInt(iквотируемых.val());
				var работающих = parseInt(iработающих.val());
				var невыполненая_квота = квотируемых - работающих;
				if ('NaN'!=невыполненая_квота.toString())
					iневыполнено.val(квотируемых - работающих);
			}
			catch (ex) { }
		}

		controller.UpdateVisibility= function()
		{
			var sel = this.binding.form_div_selector;
			var iарендованных = $(sel + ' input[model_field_name="Количество_квотируемых.арендованных"]');
			var арендованных = iарендованных.val();
			var ifarenda = $(sel + ' tr.ifarenda');
			if (арендованных && '' != арендованных && '0'!=арендованных)
			{
				ifarenda.show();
			}
			else
			{
				ifarenda.hide();
			}
		}

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return profiled_tpl({ form: this.model, text_report_period: h_profile.text_report_period });
		}

		controller.GetFormContent= function()
		{
			var res = base_GetFormContent.call(this);
			if (res && res.Арендодатель_рабочих_мест &&
				(!res.Количество_квотируемых
				|| !res.Количество_квотируемых
				|| !res.Количество_квотируемых.арендованных
				|| '' == res.Количество_квотируемых.арендованных
				|| '0' == res.Количество_квотируемых.арендованных))
			{
				if (res.Арендодатель_рабочих_мест)
					delete res.Арендодатель_рабочих_мест;
			}
			return res;
		}

		var xml_codec = codec_xml();
		xml_codec.schema = { tagName: 'Выполнение_квоты_для_приёма_инвалидов' };
		controller.UseCodec(xml_codec);

		return controller;
	}
});
